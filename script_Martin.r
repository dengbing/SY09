setwd("~/UTC/SY09/SY09")
origins_list <- read.table("donnees/recettes-pays.data", header = TRUE, sep = ",")
origins_matrix <- as.matrix(origins_list[,-1], dimnames = list(origins_list$origin, colnames(origins_list)))
rownames(origins_matrix)<-origins_list$origin

# -1-- ANALYSE ---------------------------------------------------------------------------------

# Ingrédients les plus/les moins utilisés par pays
most_used_ing <- cbind(rownames(origins_matrix), colnames(origins_matrix)[apply(origins_matrix, 1, which.max)])
most_used_ing[order(most_used_ing[,2]),]

less_used_ing <- cbind(rownames(origins_matrix), colnames(origins_matrix)[apply(origins_matrix, 1, which.min)])
less_used_ing[order(less_used_ing[,2]),]

# On se concentre sur les origines, donc pays -> variables, ingrédients -> individu
torigins_matrix <- t(origins_matrix)
png(filename="plots/countries.png", width = 4800, height = 4800)
pairs(torigins_matrix)
dev.off()

# TODO : Boites à moustache, histogramme -> résumé de la dispersion des données


# -2-- ACP ---------------------------------------------------------------------------------
# Princomp ne peut pas être utilisé sur origins_matrix car il y a plus de variable que d'individus
# On applique l'ACP avec les pays en tant que variables : T(origins_matrix)

ACP_res <- princomp(torigins_matrix)
summary(ACP_res)
(ACP_res$sdev)^2 # Les valeurs propres
ACP_res$loadings # Les vecteurs propres
ACP_res$scores # Le nouveau tableau individus-variables

# Plot ACP
png(filename="plots/ACPvecteurs.png", width = 480, height = 480)
plot(ACP_res)
dev.off()
png(filename="plots/ACPplan23.png", width = 480, height = 480)
biplot(ACP_res, c(2,3))
dev.off()
png(filename="plots/ACPplan12.png", width = 480, height = 480)
biplot(ACP_res, c(1,2))
dev.off()


# -3-- CAH ---------------------------------------------------------------------------------

# Noms des pays en labels
rownames(origins_list)<-origins_list$origin
origins_list <- origins_list[,-1]

# Construction du dendogramme
d_origins <- dist(origins_list, , method = "manhattan")
hc_origins_complete <- hclust(d_origins, method = "complete")
hc_origins_single <- hclust(d_origins, method = "single")
hc_origins_ward <- hclust(d_origins, method = "ward.D")
hc_origins_average <- hclust(d_origins, method = "average")

dend_complete <- rotate(hc_origins_complete, order = rownames(origins_list))
dend_single <- rotate(hc_origins_single, order = rownames(origins_list))
dend_ward <- rotate(hc_origins_ward, order = rownames(origins_list))
dend_average <- rotate(hc_origins_average, order = rownames(origins_list))

dend_complete <- color_branches(dend_complete, k=4)
dend_single <- color_branches(dend_single, k=4)
dend_ward <- color_branches(dend_ward, k=4)
dend_average <- color_branches(dend_average, k=4)


png(filename="plots/DendogramsComp.png", width = 800, height = 1600)
par(mfrow=c(4,1))
plot(dend_complete, main="Complete")
plot(dend_single, main="Single")
plot(dend_average, main="Average")
plot(dend_ward, main="Ward")
dev.off()
# We can notice that the ward method gives a more effective classification


# -4-- K-means ---------------------------------------------------------------------------------
#TODO


# -5-- Comparaison Classifications/Géographie ---------------------------------------------------
library(dendextend)

# Ajout des continents
origins_list$continent <- factor(c("africa","america","asia","america","america","asia","europa","europa","europa","europa","europa","asia","europa","europa","asia","asia","europa","america","asia","africa","europa","america","america","europa","asia","asia"),
                         levels=c("europa","asia","america","africa"))
origins_list[,c("origin","continent")]

origins_continents <- rev(levels(origins_list$continent))

#affiche dendrogramme en fonction des esp?ces de fleur (couleurs)
# Manually match the labels, as much as possible, to the real classification of the flowers:
labels_colors(dend) <-
  c("red","green","blue")[sort_levels_values(
    as.numeric(iris[,5])[order.dendrogram(dend)]
  )]

# We shall add the flower type to the labels:
labels(dend) <- paste(as.character(iris[,5])[order.dendrogram(dend)],
                      "(",labels(dend),")", 
                      sep = "")
# We hang the dendrogram a bit:
dend <- hang.dendrogram(dend,hang_height=0.1)
# reduce the size of the labels:
# dend <- assign_values_to_leaves_nodePar(dend, 0.5, "lab.cex")
dend <- set(dend, "labels_cex", 0.5)
# And plot:
par(mar = c(3,3,3,7))
plot(dend, 
     main = "Clustered Iris data set
     (the labels give the true flower species)", 
     horiz =  TRUE,  nodePar = list(cex = .007))
legend("topleft", legend = iris_species, fill = rainbow_hcl(3))



#Graph: utilisation de olive_oil
barplot(origins_matrix[order(origins_matrix[,1],decreasing=TRUE),][,1], beside = TRUE, col = rainbow(nrow(origins_matrix)), names.arg = toupper(rownames(origins_matrix)), las=2)